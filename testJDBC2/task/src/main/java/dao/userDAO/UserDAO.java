package dao.userDAO;

import model.User;

public interface UserDAO {
    public User create(User user);

    boolean checkLog(String login);

    String checkRole(String login);
}
