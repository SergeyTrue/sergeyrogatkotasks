package dao.userDAO;

import dao.JDBCConnection;
import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserClass implements UserDAO {

    private final static String INSERT = "INSERT INTO users (login, password, role) VALUES (?,?,?)";
    private final static String SELECT_LOGIN = "SELECT login FROM users WHERE login =?";
    private final static String SELECT_ROLE = "SELECT role FROM users WHERE login =?";

    @Override
    public User create(User user) {
        try (PreparedStatement preparedStatement = JDBCConnection.getConnection().prepareStatement
                (INSERT)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, "USER");
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean checkLog(String login) {
        boolean result = false;
        try (PreparedStatement preparedStatement = JDBCConnection.getConnection().prepareStatement
                (SELECT_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("login");
                result = name != null;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String checkRole(String login) {
        String result = null;
        try (PreparedStatement preparedStatement = JDBCConnection.getConnection().prepareStatement
                (SELECT_ROLE)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String role = resultSet.getString("role");
                result = role;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }
}
