package dao.rectangleDAO;

import dao.JDBCConnection;
import model.Rectangle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RectangleClass implements RectangleDAO {

    private final static String INSERT = "INSERT INTO rectangl (height, length, result, login)  VALUES(?,?,?,?)";
    private final static String SELECT = "SELECT (height, length, result, id, login) FROM rectangl WHERE login=?";
    private final static String DELETE = "DELETE FROM rectangl WHERE id=?";

    @Override
    public void rectangleCreate(double height, double length, double result, String login) {
        try (PreparedStatement preparedStatement = JDBCConnection.getConnection().prepareStatement
                (INSERT)) {
            preparedStatement.setDouble(1, height);
            preparedStatement.setDouble(2, length);
            preparedStatement.setDouble(3, result);
            preparedStatement.setString(4, login);
            preparedStatement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Rectangle> read(String s) {
        ArrayList<Rectangle> rectangles = new ArrayList<>();
        try (PreparedStatement preparedStatement = JDBCConnection.getConnection().prepareStatement
                (SELECT)) {
            preparedStatement.setString(1, s);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                double height = resultSet.getDouble("height");
                double length = resultSet.getDouble("length");
                double result = resultSet.getDouble("result");
                int id = resultSet.getInt("id");
                rectangles.add(new Rectangle(height, length, result, id));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return rectangles;
    }

    @Override
    public void deleteRectangle(Integer id) {
        try (PreparedStatement preparedStatement = JDBCConnection.getConnection().prepareStatement
                (DELETE)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

