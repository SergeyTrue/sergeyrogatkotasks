package dao.rectangleDAO;

import model.Rectangle;

import java.util.ArrayList;

public interface RectangleDAO {
    void rectangleCreate(double height, double length, double result, String login);

    ArrayList<Rectangle> read(String s);

    void deleteRectangle(Integer id);
}
