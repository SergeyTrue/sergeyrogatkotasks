package model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class User {
    private String login;
    private String password;
    private ROLE role;
    public enum ROLE {
        USER, ADMIN
    }
}
