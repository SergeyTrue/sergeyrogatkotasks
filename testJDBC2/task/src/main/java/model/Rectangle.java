package model;

import lombok.*;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Data
@NoArgsConstructor
public class Rectangle {
    double height;
    double length;
    double result;
    int id;
    String login;

    public Rectangle(double height, double length, double result, int id) {

    }
}
