package servlets;

import dao.userDAO.UserClass;
import dao.userDAO.UserDAO;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/role")
public class Role extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        HttpSession session = req.getSession();
        session.setAttribute("login", login);
        UserDAO user = new UserClass();
        String checkRole = user.checkRole(login);

        if (user.checkLog(login)) {
            if (checkRole.equalsIgnoreCase(String.valueOf(User.ROLE.USER))) {
                req.getRequestDispatcher("user.jsp").forward(req, resp);
            } else if (checkRole.equalsIgnoreCase(String.valueOf(User.ROLE.ADMIN))) {
                req.getRequestDispatcher("admin.jsp").forward(req, resp);
            }
        } else if (!user.checkLog(login)) {
            req.getRequestDispatcher("error.jsp").forward(req, resp);
        }
    }
}
