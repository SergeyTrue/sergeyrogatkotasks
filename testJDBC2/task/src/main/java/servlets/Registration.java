package servlets;

import dao.userDAO.UserClass;
import model.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/reg")
public class Registration extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        ServletContext servletContext = getServletContext();
        servletContext.setAttribute("login", login);
        servletContext.setAttribute("password", password);
        UserClass userClass = new UserClass();
        if (!userClass.checkLog(login)) {
            userClass.create(new User(login, password, User.ROLE.USER));
            servletContext.getRequestDispatcher("/index.jsp").forward(req, resp);
        } else {
            servletContext.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}

