package servlets;

import dao.rectangleDAO.RectangleClass;
import dao.rectangleDAO.RectanngleСalculation;
import model.Rectangle;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet("/area")
public class AreaRectangle extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        double height = Double.parseDouble(req.getParameter("height"));
        double length = Double.parseDouble(req.getParameter("length"));

        ServletContext servletContext = getServletContext();
        servletContext.setAttribute("height", height);
        servletContext.setAttribute("length", length);

        RectanngleСalculation сalculation = new RectanngleСalculation();
        double result = сalculation.number(height, length);
        servletContext.setAttribute("result", result);

        HttpSession session = req.getSession();
        String log = (String) session.getAttribute("login");

        RectangleClass rectangleClass = new RectangleClass();
        rectangleClass.rectangleCreate(height, length, result, log);

        ArrayList<Rectangle> rectangles = rectangleClass.read(log);
        req.setAttribute("rectangles", rectangles);


        servletContext.getRequestDispatcher("/user.jsp").forward(req, resp);
    }
}
