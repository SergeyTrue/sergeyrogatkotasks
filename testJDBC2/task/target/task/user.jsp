 <%@ page contentType="text/html;charset=UTF-8" language="java" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <html>
 <body>
<h1> Rectangle Area </h1>
        <form action="area" method="POST" >
            <input type="text" required placeholder="height" name="height" ></p>
            <input type="text" required placeholder="length" name="length"></p>
        <p> <input class="button" type="submit" value="OK"></p>
        </form>
<h2>Archive: </h2> <br>
<blockquote>
<table border="1">
      <thead>
        <td><b> id </b></td>
        <td><b> height </b></td>
        <td><b> length </b></td>
        <td><b> result </b></td>
        <td><b> x </b></td>
      </thead>
<c:forEach var="rectangle" items="${rectangles}">
      <tr>
        <td>${rectangle.id} </td>
        <td>${rectangle.height} </td>
        <td>${rectangle.length} </td>
        <td>${rectangle.result} </td>
        <td> <form method="POST" action='<c:url value="/delete" />' style="display:inline;">
            <input type="hidden" name="id" value="${rectangle.id}">
            <input type="submit" value="Delete">
        </form></td>
      </tr>
</c:forEach>

</table>
</blockquote>

<a href="<c:url value="/logout"/>"> Logout</a>
 </body>
 </html>
