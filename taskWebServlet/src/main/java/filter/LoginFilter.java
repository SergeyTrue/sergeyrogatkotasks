package filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


@WebFilter("/login")
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String password = (servletRequest).getParameter("123");
        String login = (servletRequest).getParameter("login");
        if (password == null || !password.equals("123")) {
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("/error.jsp");
            requestDispatcher.forward(servletRequest, servletResponse);
        } else if (login == null || !login.equals("login")) {
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("/error.jsp");
            requestDispatcher.forward(servletRequest, servletResponse);
        } else
            filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
