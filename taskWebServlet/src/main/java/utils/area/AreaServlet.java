package utils.area;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/area")
public class AreaServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Integer num1 = Integer.parseInt(req.getParameter("num1"));
            Integer num2 = Integer.parseInt(req.getParameter("num2"));
            Integer result = num1 * num2;
            req.setAttribute("result", result);
            req.getRequestDispatcher("areaResult.jsp").forward(req, resp);
        } catch (Exception e) {
            req.getRequestDispatcher("fail.jsp").forward(req, resp);
        }
    }
}

